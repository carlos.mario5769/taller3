package com.ecci.taller3.model

data class Product(var name: String, var description: String, val stock: Int)